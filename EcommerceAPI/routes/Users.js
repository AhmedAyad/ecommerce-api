const { User, validateUser, validateEditUser } = require("../models/user");
const express = require("express");
const mongoose = require("mongoose");
const _ = require("lodash");
//const bcrypt = require("bcrypt");
const bcrypt = require('bcryptjs');

const jwt = require("jsonwebtoken");
const router = express.Router();
const Joi = require("joi");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
// const fileFilter = (req, file, cb) => {
//   if (file.mimeType === "image/jpg" || file.mimeType === "image/png") {
//     cb(null, true);
//   } else {
//     cb(null, false);
//   }
// };
const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 5 },
  // fileFilter:fileFilter
});

router.get("/", async (req, res) => {
  const user = await User.find();
  //console.log(user);
  res.send(user);
});

router.get("/:id", async (req, res) => {
  const user = await User.findById(req.params.id);
  if (!user) res.status(404).send("user not found");
  res.send(user);
});

router.post("/", upload.single("userImage"), async (req, res) => {
  //console.log(req.file);
  const { error } = validateUser(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).send("user is already registered ");
  user = new User({
    // _.pick(req.body, ["name", "email", "gender", "password"] , req.file.path)
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    gender: req.body.gender,
    userImage: req.file.path,
  });
  //console.log(req.file);

   const salt = await bcrypt.genSalt(10);
   user.password = await bcrypt.hash(user.password, salt);
  
  user = await user.save();
   const token = jwt.sign({ _id: this.id }, "jwtPrivateKey");

  res
    .header("x-auth-token", token)
    .send(
      // _.pick(user, ["_id", "name", "email", "userImage", "gender", "password"])
      user
    );
});

router.put("/:id", upload.single("userImage"),async (req, res) => {
  let user = await User.findById(req.params.id);
  if (!user) res.status(404).send("user not found");
  //const { error } = validateEditUser(req.body);
  //if (error) res.status(400).send(error.details[0].message);

  user.name = req.body.name;
  user.email = req.body.email;
  console.log("Old password is ",req.body.password);
   if(user.password!==req.body.password)
   {
        const salt = await bcrypt.genSalt(10);
       user.password = await bcrypt.hash(req.body.password, salt);
   }
   else
   {
    user.password = req.body.password;
   }

  user.userImage = req.file.path;
  user.gender = req.body.gender;

  // const salt = await bcrypt.genSalt(10);
  // user.password = await bcrypt.hash(user.password, salt);
  // user = await user.save();
  // const token = jwt.sign({ _id: this.id }, "jwtPrivateKey");


  user = await user.save();
  res.send(user);
});

module.exports = router;
