const express = require("express");
const mongoose = require("mongoose");
const authorization = require("../middleware/authorization");
const Joi = require("joi");
const _ = require("lodash");
//const bcrypt = require("bcrypt");
const bcrypt = require('bcryptjs');

const jwt = require("jsonwebtoken");
const config = require("config");
const { User } = require("../models/user");

const router = express.Router();

router.get("/me",authorization, async (req, res) => {
  // console.log("rqqqqqqqqqqq", req);
  // console.log("resssssssssssssss", res);
  const user = await User.findById(req.user._id);
  res.send(user);
});

router.post("/", async (req, res) => {
  const { error } = validateRequest(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).send("Invalid email or password");

  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword) return res.status(400).send("Invalid email or password");

  //   const token = jwt.sign({ _id: user.id }, config.get("jwtPrivateKey"));
  // const token = user.generateAuthTpken(); 
  const token = jwt.sign({_id :user._id} , "jwtPrivateKey");
  res.send({token:token});
});

function validateRequest(req) {
  const schema = {
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required(),
  };
  return Joi.validate(req, schema);
}

module.exports = router;
