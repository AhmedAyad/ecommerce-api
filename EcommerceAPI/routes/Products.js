const { Product, validateProduct } = require("../models/product");
const auth = require("../middleware/authorization");
const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const Joi = require("joi");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 5 },
  // fileFilter:fileFilter
});


//add one router for get promotions 
router.get("/Promotions/", async (req, res) => {
  const product = await Product.find({promotionprecentage:{$gt:0}});
  console.log(product);
  res.send(product);
});

router.delete("/:id",async (req, res) => {
  //const product = await Product.findById(req.params.id);
   const product = await Product.findByIdAndDelete(req.params.id);
     res.send(product);
})


router.get("/", async (req, res) => {
  const product = await Product.find();
  // console.log(product);
  res.send(product);
});

router.get("/:id", async (req, res) => {
  const product = await Product.findById(req.params.id);
  if (!product) res.status(404).send("user not found");
  res.send(product);
});

router.post("/", upload.single("imagePath"), async (req, res) => {
  const { error } = validateProduct(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  let product = new Product({
    title: req.body.title,
    description: req.body.description,
    quantity: req.body.quantity,
    price: req.body.price,
    imagePath: req.file.path,
    promotionprecentage:req.body.promotionprecentage
  });

  product = await product.save();
  res.send(product);
});

router.put("/:id" ,upload.single("imagePath"), async (req, res) => {
  let product = await Product.findById(req.params.id);
  if (!product) res.status(404).send("product not found");
  const { error } = validateProduct(req.body);
  if (error) res.status(400).send(error.details[0].message);

  product.title = req.body.title;
  product.description = req.body.description;
  product.quantity = req.body.quantity;
  product.imagePath = req.file.path;
  product.price = req.body.price;
  product = await product.save();
  res.send(product);

});

module.exports = router;
