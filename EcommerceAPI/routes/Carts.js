const { Cart, validateCart} = require("../models/cart");
const {User} = require('../models/user')
const {Product} = require('../models/product');
var assert = require('assert');


const express = require("express");
const mongoose = require("mongoose");

const router = express.Router();
const Joi = require("joi");

 //Helper Function for calculate tootal price 
 function calTotalPrice(cart)
 {
  let totalPaid = 0;
  if(cart.products.length>0)
  {
    cart.products.forEach(  (element,index) => {
        totalPaid += cart.products[index].totalprice;
    })

  }

 return totalPaid;
 }

 //Helper function to update cart
async function  addproductToCart( cart,prodID)
{

 
  let productISFound = false;
   
  if(cart.products.length>0)
  {
      cart.products.forEach((element,index) => {
        // console.log(`${element._id}`)
        // console.log(`${prodID}`);
     
         if(JSON.stringify(element._id) == JSON.stringify(prodID))
             { 
              //  console.log("equal")
                cart.products[index].quantity++;
                cart.products[index].totalprice = (cart.products[index].quantity* cart.products[index].price);
                productISFound = true;
                // console.log(productISFound)
            }
            // else console.log("not equal")
      });
  }
   
   if(productISFound==false)
   {
     console.log("here ya awlad");
      const product = await Product.findById(prodID);
      if (!product) return res.status(400).send('Invalid product.');
      else {product.totalprice = product.price
             product.quantity = 1;
             };
      cart.products.push(product);
   }

    cart.cartprice = await calTotalPrice(cart);
    await cart.cartQuantity++;
  cart = await cart.save();
  //console.log(cart);
  return cart;
   //res.send(cart);

}






router.get("/",async(req,res)=>{
  const cart = await Cart.find();
  res.send(cart);
})



 // get current cart for custome user so send id for user

router.get("/user/:id", async (req, res) => {
    const cart = await Cart.findOne({$and:[{user:{$eq:req.params.id}},{status:{$eq:"InCart"}}]})
    //if (!cart) return res.status(404).send("no cart found");
    res.send(cart);
  });




  router.get("/:id",async (req,res)=>{
    const cart = await Cart.findById(req.params.id).populate("user");
    if (!cart) res.status(404).send("no cart found");
    res.send(cart);
  })

 //get quantity 
  router.get("/user/quantity/:id",async (req,res)=>{
    var cartQ = {quantity:0};
    const cart = await Cart.findOne({$and:[{user:{$eq:req.params.id}},{status:{$eq:"InCart"}}]})
    if (cart)  cartQ.quantity = await cart.cartQuantity ;
    console.log(`Quantity is ${cartQ.quantity}`)
     
      res.send(cartQ);
  })





  // update cart for cart id 
  router.patch("/:id",async(req,res)=>{
    console.log("now iam in patch request");
    const {id} = req.params;
    let cart = await Cart.findById(id);
    if(!cart) return res.status(400).send("not valid as cart id");
    let productISFound = false;
   
    if(cart.products.length>0)
    {
        cart.products.forEach(  (element,index) => {
           if( element._id == req.body.product)
               { 
                  cart.products[index].quantity++;
                  cart.products[index].totalprice = (cart.products[index].quantity* cart.products[index].price);
                  productISFound = true;
                  console.log(productISFound)
              }
        });
    }

     if(productISFound===false)
     {
        const product = await Product.findById(req.body.product);
        if (!product) return res.status(400).send('Invalid product.');
        else {product.totalprice = product.price
               product.quantity = 1;
              };
        cart.products.push(product);
     }
  
      cart.cartprice = await calTotalPrice(cart);
    cart = await cart.save();
    res.send(cart);
  })
  

  router.post('/',async (req,res)=>{
    
    const { error } = validateCart(req.body); 
  if (error)
  {  
     return res.status(400).send(error.details[0].message);
  }


 const currentUser = await User.findById(req.body.user);

  if (!currentUser) return res.status(400).send('Invalid user.');
  
  const product = await Product.findById(req.body.product);
  if (!product) return res.status(400).send('Invalid product.');
  
  
  const currentCart = await Cart.findOne({$and:[{user:{$eq:currentUser._id}},{status:{$eq:"InCart"}}]})  
  if(currentCart !== null)
     res.send(addproductToCart(currentCart,product._id));
   else
   {  
    product.totalprice = product.price
     let cart = new Cart({ 
         user: {
            _id:currentUser._id
      //     // _id: currentUser._id,
       },
       
         product:product._id,
         cartUser:currentUser,
         cartprice:product.price
        });
         
        
      // console.log(`so it is ${cart.products[0]}`)
         cart.products.push(product);
        //await cart.products.push(product);
       await cart.cartQuantity++;
        cart = await cart.save();
        
     res.send(cart);
      }
});

module.exports = router;
