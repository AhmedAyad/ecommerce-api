const { Order, validateOrder} = require("../models/order");
const {User} = require('../models/user')
const {Product} = require('../models/product');
const {Cart} = require('../models/cart');

const express = require("express");
const mongoose = require("mongoose");

const router = express.Router();
const Joi = require("joi");


//for admin get all pending order 
// router.get("/Pendings/",async (req,res)=>{
   
//   const orders = await Order.find({orderStatus:{$eq:"pending"}}).populate("cart");
//    if(!orders.length>0) return res.status(400).send("No Pending Orders");

//   res.send(orders);


// })



//get all orders
router.get("/",async (req,res)=>{
  const orders = await Order.find().populate("cart").populate("user");//.populate('products','title-_id').populate('user' ,'name-_id').populate("carts");
  console.log(orders);
  if(!orders.length>0) return res.status(400).send("No Orders");
 res.send(orders);

})

// get order for id  
router.get('/:id',async (req,res)=>{
  const {id} = req.params;
  const order = await Order.findById(id).populate("cart").populate("user");
  if(!order) return res.status(400).send("No Order with this id");
 res.send(order);

});



//checkout to make order from user
router.post('/',async (req,res)=>{

    const { error } = validateOrder(req.body); 
  if (error) return res.status(400).send(error.details[0].message);

   const cart = await Cart.findById(req.body.cart).populate("user");
   if (!cart) return res.status(400).send('Invalid Cart.');

     let order = new Order({ 
         cart:cart 
        });

      cart.status = "OutCart";
      await cart.save();  
      order = await order.save();
     res.send(order);
    
});


// get all orders for user  // it must decleare in user 
router.get('/user/:id',async (req,res)=>{
  const userID = req.params.id;

 
  // const orders = await Order.find({"cart.user":{$eq:id}}).populate("cart");
  let orders = [];
  const allorders = await Order.find().populate("cart")
  allorders.forEach(element => {
    //console.log(element.cart.user);
    if(element.cart.user == userID )
    {console.log("yses iam ")
      orders.push(element)
    }
  });
  
  
  //const orders = await Order.find({"cart.user":{ $eq: userID}}).populate("cart")

  //console.log(`order is  ${orders}`)
  if(!orders.length>0) return res.status(400).send("No Orders for this user");
    res.send(orders);
});





//update order status 
router.patch('/:id', async (req, res) => {
    const id = req.params.id;
    console.log(id);
    let order = await Order.findById(id);
    console.log(order);
    order.orderStatus = req.body.orderStatus;
    order = await order.save();
    res.send(order);
    });
    

module.exports = router;


