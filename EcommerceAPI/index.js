const express = require("express");
const halmet = require('helmet');
const comprssion = require('compression')


const mongoose = require("mongoose");
const Joi = require("joi");
const users = require("./routes/Users");
const products = require("./routes/Products");
const carts = require("./routes/Carts");
const auth = require("./routes/auth");
const config = require('config');
// const cors = require('cors');
const orders = require('./routes/Orders')
const cors = require('cors')

const app = express();
app.use(express.json());
app.use(cors());

app.use("/api/users", users);
app.use("/api/products", products);
app.use("/api/auth", auth);
app.use("/api/orders",orders);
 app.use("/api/carts",carts);
 app.use("/uploads", express.static("uploads"));
 app.use(halmet);
 app.use(comprssion);
 
        


const connectionURL = process.env.MONGODB_URI||'mongodb://localhost/ecommerce';
const port = process.env.PORT||3000;

mongoose
  .connect(connectionURL,{useNewUrlParser:true,useUnifiedTopology:true})
  .then(() => console.log("connected to db..."))
  .catch((err) => console.log(err.message));

app.listen(port, () => {
  console.log("App listening on port 3000!");
});
