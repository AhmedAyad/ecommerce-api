const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const cartSchema = require('./cart').cartSchema;
const mongoose = require('mongoose');



const Order = mongoose.model('Order',new mongoose.Schema({

   cart:{type:mongoose.Schema.ObjectId,
    required:true,
   ref:'Cart'},

 orderDate: { 
     type: Date, 
     default: Date.now
   },

orderStatus:{
   type:String,
   enum:['accepted','rejected','pending','cancelled'],
   default:'pending'
},

orderPrice:{
      type:Number
}

}))

function validateOrder(order) {

    const schema = {
      cart:Joi.objectId().required(),
      user:Joi.objectId(),
      product:Joi.objectId(),
      products:Joi.array().items(Joi.object({ title: Joi.string().min(7).max(255).required(),
          description: Joi.string().max(255).required(),
          quantity: Joi.number(),
          price: Joi.number(),
          imagePath: Joi.string(),
          promotionprecentage:Joi.number() })).allow(null),
      productPrices:Joi.array().items(Joi.number()),
      productQuentity:Joi.array().items(Joi.number()),
      cartprice:Joi.number(),
      status:Joi.string(),
      orderDate:Joi.date(),
      orderStatus:Joi.string(),
      orderPrice:Joi.number()
    };
    return Joi.validate(order, schema,{abortEarly:false});
  }
  

  exports.Order = Order;
  exports.validateOrder = validateOrder;