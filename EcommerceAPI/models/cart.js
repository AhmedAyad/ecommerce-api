const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const productSchema = require('./product').productSchema;
const userSchema = require('./user').userSchema;

const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
    user:{
        type:mongoose.Schema.Types.ObjectId,
        //type:userSchema,
        //require:true,
        ref:'user'
    },
     product:{type:mongoose.Schema.Types.ObjectId,unique:false,
         ref:'product',
        },
       
        cartUser:{type:{},unique:false,
        //     //ref:'user',
            },

    products:[{
        type:productSchema,
        ref:'product', 
        unique:false,
       
 }],
  cartprice:{type:Number},
    status:{type:String,
     default:"InCart"},
  cartQuantity:{type:Number,default:0}

}) 




const Cart = mongoose.model('Cart',cartSchema);


function validateCart(cart) {

    const schema = {
        user: Joi.objectId().required(),
        product:Joi.objectId(), 
        cartprice:Joi.number(),
        status:Joi.string(),
        cartQuantity:Joi.number()
    };
    return Joi.validate(cart, schema,{abortEarly:false});
  }

  exports.Cart = Cart;
  exports.cartSchema = cartSchema;



  exports.validateCart = validateCart;
