const Joi = require("joi");
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  title: { type: String, required: true,unique:false },
  description: {
    type: String,
    required: false,
  },
  quantity: {type:Number,default:1},
  price: { type: Number, required: true },
   totalprice:{type:Number,required:false,default:0},
   imagePath: { type: String, required: true },
  //this 1 attributes for add promotions for custom Product 
  promotionprecentage:{type:Number,default:0}
})


const Product = mongoose.model(
  "product",productSchema
);

function validateProduct(product) {
  const schema = {
    title: Joi.string().min(7).max(255).required(),
    description: Joi.string().max(255).required(),
    quantity: Joi.number(),
    price: Joi.number(),
    totalprice:Joi.number(),
    imagePath: Joi.string(),
    promotionprecentage:Joi.number()
  };
  return Joi.validate(product, schema);
}

exports.Product = Product;
exports.productSchema = productSchema;
exports.validateProduct = validateProduct;
