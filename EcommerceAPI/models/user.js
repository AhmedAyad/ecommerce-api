const Joi = require("joi");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    minlength: 7,
    maxlength: 255,
    required: true,
  },
  userImage: String,
  gender:String , 
  email: {
    type: String,
    unique: true,
    minlength: 5,
    maxlength: 255,
    match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    required: true,
  },
  password: {
    type: String,
    minlength: 5,
    maxlength: 1024,
    required: true,
  },
  role: { type: String, default: "Normal User" },
});
const User = mongoose.model("user", userSchema);

userSchema.methods.generateAuthTpken = function () {
  const token = jwt.sign({ _id: this.id }, "jwtPrivateKey");
  return token;
};
function validateUser(user) {
  const schema = {
    name: Joi.string().min(7).max(255).required(),
    email: Joi.string().email().max(255).required(),
    password: Joi.string().min(5).max(255).required(),
    gender:Joi.string(),
    userImage:Joi.string(),
    role: Joi.string(),
  };
  return Joi.validate(user, schema);
}

function validateEditUser(){
  const schema = {
    name: Joi.string().min(7).max(255), 
    email:Joi.string().email().max(255),
    password: Joi.string().min(5).max(255), 
    gender:Joi.string(),
    userImage:Joi.string(),
    role: Joi.string(),
  }
}

exports.User = User;
exports.userSchema = userSchema;
exports.validateUser = validateUser;
exports.validateEditUser = validateEditUser;
