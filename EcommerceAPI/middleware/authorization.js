const jwt = require("jsonwebtoken");

function authorization(req, res, next) {
  // const token = req.header("x-auth-token");
  const token = req.header("token");  
  if (!token) return res.status(401).send("Access denied");
  try {
    const decoded = jwt.verify(token, "jwtPrivateKey");
    req.user = decoded;
    next();
  } catch (ex) {
    res.status(400).send("invalid token");
  }
}

module.exports = authorization;
